# Installing GitLab Runner

## Prerequisites
- GitLab account ที่มีสิทธิ์ในการ register runner ใน Group/Sub group/Project



## Installation on macOS

### Step 1: Install Homebrew (if not already installed)

ดาวน์โหลด Homebrew ผ่าน command

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
### Step 2: Install GitLab Runner
ใช้ Homebrew ติดตั้ง GitLab Runner.

```bash
brew install gitlab-runner
```

### Step 3: Start GitLab Runner
#### Start the GitLab Runner service.

```bash
sudo gitlab-runner start
```

### Step 4: Check GitLab Runner status

#### Check Gitlab Runner status using comand
```bash
sudo gitlab-runner status
```

## Installation on Windows
### Step 1: Download GitLab Runner
Go to the GitLab Runner [download](https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/binaries/gitlab-runner-windows-amd64.exe) page and download the latest version.

### Step 2: Create Gitlab-foler
สร้างโฟลเดอร์สำหรับ gitlab-runner ในเครื่อง เช่น C:\GitLab-Runner.

### Step 3: Rename gitlab-runner file and copy to gitlab-runner folder
เปลี่ยรชื่อไฟล์ binary ของ gitlab-runner ที่ดาวน์โหลดมาให้เป็น gitlab-runner.exe และย้ายเข้าไปในโฟลเดอร์ C:\GitLab-Runner
### Step 4: Install Runner

เปิด Command Prompt as an administrator และเข้าไปในโฟลเดอร์ gitlab-runner ด้วย command

```cmd
cd "C:\GitLab-Runner"
```
#### Install GitLab Runner.

```cmd
gitlab-runner install
```
### Step 5: Start GitLab Runner

#### Start the GitLab Runner service.

```cmd
gitlab-runner start
```
### Step 6: Check GitLab Runner status

#### Check Gitlab Runner status using comand
```cmd
gitlab-runner status
```

## Register Gitlab-runner
เข้าไปที่โปรเจคของเราแล้วไปที่ Setting > CICD
<img src="image/settingcicd.png" alt="drawing" width="400"/>

เลือก Runners แล้วกดไปที่ New Project Runner 

![newrunner](image/newrunner.png)

เลือก Platform ที่เราใช้งานและใส่ tag ของ runner 

![tag](image/tagrunner.png)

tag คือเป็นชื่อที่เราจะใช้เรียก runner ตัวนั้นๆ มาใช้ใน job ของเราในที่นี้คือ "runner01"

กดที่ Create runner และทำตาม Step ของ gitlab

![runnerstep](image/runnerstep.png)