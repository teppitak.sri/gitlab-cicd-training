# LAB Advance gitlab-ci.yml

## Content
- Advance syntax
- Optimize GitLab CI/CD

# Advance Syntax

## default job

สร้าง default job ขึ้นมาและกำหนด default tag เป็น runner01
``` yaml
variables:
  ENV: prd
stages:
- test
- deploy

default:
  tags:
    - runner01

test:
  script:
    - echo "test"

deploy:
  script:
    - echo "deploy"
```
## before_script/after_script
กำหนด before_script defaukt job และ test job

### before_script
``` yaml
variables:
 ENV: prd
stages:
- test
- deploy

default:
  tags:
    - runner01
  before_script:
    - echo "default before script"

test:
  before_script:
    - echo "test before script"
  script:
    - echo "test"

deploy:
  script:
    - echo "deploy"
```
### after_script
เพิ่ม after_script และแก้ command ให้ job fail
``` yaml
variables:
 ENV: prd
stages:
- test
- deploy

default:
  tags:
    - runner01
  before_script:
    - echo "default before script"

test:
  before_script:
    - echo "test before script"
  script:
    - echo "test"
  after_script:
    - echo "test after script"

deploy:
  script:
    - echo "deploy"
    - echa "fail shua shua"
  after_script:
    - echo "deploy after script"
```
## Artifact
``` yaml
variables:
 ENV: prd
stages:
- artifact

default:
  tags:
    - runner01
  before_script:
    - echo "default before script"

artifact_file:
  stage: artifact
  script:
    - echo "artifact1" > artifact.txt
  artifacts:
    paths:
      - "*.txt"

artifact_dir:
  stage: artifact
  script:
    - mkdir artifact
    - echo "artifact2" > artifact/artifact2.txt
    - echo "artifact3" > artifact/artifact3.txt
  artifacts:
    paths:
      - artifact/
```
## dependencies
``` yaml
variables:
 ENV: prd
stages:
- artifact
- exclude
- depend

default:
  tags:
    - runner01
  before_script:
    - echo "default before script"


artifact_dir:
  stage: artifact
  script:
    - mkdir artifact
    - echo "artifact2" > artifact/artifact.txt
    - echo "artifact3" > artifact/artifact.txt
  artifacts:
    paths:
      - artifact/

exclude_artifact:
  stage: exclude
  script:
    - ls
  dependencies: []

call_artifact:
  stage: depend
  script:
    - ls
  dependencies: [artifact_dir]


```

## rules
แก้ไขไฟล์ .gitlab-ci.yml โดยกำหนด rules ของแต่ละ job และสร้าง Dockerfile

#### Dockerfile
```sh
FROM nginx
EXPOSE 80
```
#### .gitlab-ci.yml
```yaml
stages:
  - rules_if
  - rules_exist
  - rules_change

rules_if:
  stage: rules_if
  script: 
    - echo "Hello, Rules!"
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^feature/
      when: never
    - if: $CI_COMMIT_BRANCH == "main" || $CI_COMMIT_BRANCH == "prd"
      when: manual

rule_exist:
  stage: rules_exist
  script:
  - echo "Dockerfile exist"
  rules:
    - exists:
        - Dockerfile


rules_change:
  stage: rules_change
  script: 
  - echo "Dockerfile change"
  rules:
    - changes:
        - Dockerfile
      when: manual
    - when: never
```
#### ทดสอบแก้ไขไฟล์ Dockerfile แล้วดูผลลัพธ์อีกครั้ง
```sh
FROM nginx:alpine
EXPOSE 80
```
#### ทดสอบลบไฟล์ Dockerfile แล้วดูผลลัพธ์อีกครั้ง
## needs
แก้ไขไฟล์ .gitlab-ci.yml เพิ่ม needs เพื่อดูผลการทำงานของ pipeline
```yaml
variables:
  ENV: prd
stages:
  - build
  - test
  - deploy
  - rollback

default:
  tags:
    - runner01

build:
  stage: build
  script:
    - echo "build"

test:
  stage: test
  script:
    - echo "test"

deploy:
  stage: deploy
  script:
    - echo "deploy"

rollback:
  stage: rollback
  script:
    - echo "rollback"
  needs: [test]
  when: on_failure
```
ทำให้ job test เกิด error เพื่อดูผลอีกครั้ง

```yaml
variables:
  ENV: prd
stages:
  - build
  - test
  - deploy
  - rollback

default:
  tags:
    - runner01

build:
  stage: build
  script:
    - echo "build"

test:
  stage: test
  script:
    - echa "Fail"

deploy:
  stage: deploy
  script:
    - echo "deploy"
  needs: [test]

rollback:
  stage: rollback
  script:
    - echo "rollback"
  needs: [test]
  when: on_failure
```

เพิ่ม job scan และ สร้าง stage scan ใน gitlab-ci.yml และกำหนด needs

```yaml
variables:
 ENV: prd
stages:
  - build
  - test
  - deploy
  - rollback
  - scan
default:
  tags:
    - runner01

build:
  stage: build
  script:
    - echo "build"

test:
  stage: test
  script:
    - echo "test"

deploy:
  stage: deploy
  script:
    - echo "deploy"

rollback:
  stage: rollback
  script:
    - echo "rollback"
  needs: [test]
  when: on_failure

scan:
  stage: scan
  script:
    - echo "scan"
  needs: []
```
# Optimize Gitlab CICD
## Anchors

ใน shell script จะมรสิ่งที่เรียกว่า anchor ที่ช่วยทำให้เราลดการเขียน script เดิมซ้ำๆโดยเราจะสร้าง job ที่เป็น anchor ในไฟล์ .gitlab-ci.yml เป็นดังนี้

```yaml
# Define the anchor job

stages:
  - build
  - test
  - deploy

default:
  tags:
    - runner01

.anchor_job: &my_anchor_job
  script:
    - echo "This is my anchor job using by job $CI_JOB_NAME"

.anchor_deploy_job: &my_anchor_deploy_job
  script:
    - echo "Deploy to $ENV"


build_job:
  stage: build
  <<: *my_anchor_job 

test_job:
  stage: test
  <<: *my_anchor_job 

deploy_job_dev:
  stage: deploy
  variables:
    ENV: dev
  <<: *my_anchor_deploy_job
  
deploy_job_prd:
  stage: deploy
  variables:
    ENV: prd
  <<: *my_anchor_deploy_job
```

## Include

เราสามารถ include job จากไฟล์ yaml ที่อยู่ใน local repository ของโปรเจคหรือ จากโปรเจคที่เรามีสิทธิ์ในการเข้าถึงได้

ทำการสร้างไฟล์ build.yml ขึ้นมา
```yaml
## build.yml
build:
  stage: build
  script:
    - echo "build here"
```
แก้ไขไฟล์ .gitlab-ci.yml
```yaml
include:
  - local: 'build.yml'

default:
  tags:
    - runner01

stages:
  - build
  - test
  - deploy

test:
  stage: test
  script:
    - echo "test"

deploy:
  stage: deploy
  script:
    - echo "deploy"
```

## Build and push image to gitlab container registry

1. Install docker command to your gitlab-runner if doesn't have one

2. Update Dockerfile in your repository

#### Dockerfile
```
FROM nginx:alpine
EXPOSE 80
```

3. Update your .gitlab-ci.yml
```yaml
variables:
  IMAGE_TAG: "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG.$CI_COMMIT_SHORT_SHA"

stages:
  - build
  - release

default:
  tags:
    - runner01

build-docker:
  stage: build
  script:
    - docker login https://registry.gitlab.com -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD
    - docker build . -t $IMAGE_TAG

release-gitlab:
  stage: release
  script:
    - echo "Start Job $CI_JOB_NAME"
    - docker login https://registry.gitlab.com -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD
    - docker push $IMAGE_TAG
```

เข้าไปดู image ที่ทำการ push เก็บไว้ที่ Deploy > Container Registry 

![Alt text](image/container_registry.png)